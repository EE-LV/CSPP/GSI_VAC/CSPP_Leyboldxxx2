This LabVIEW library contains the _CSPP\_Leyboldxxx2 Actor_ derived from _CSPP\_DeviceActor_ baseclass.

Related documents and information
=================================
- README.md
- EUPL v.1.1 - Lizenz.pdf
- Contact: H.Brand@gsi.de or D.Neidherr@gsi.de
- Download, bug reports... : [https://git.gsi.de/EE-LV/CSPP/GSI_VAC/CSPP_Leyboldxxx2](https://git.gsi.de/EE-LV/CSPP/GSI_VAC/CSPP_Leyboldxxx2)
- Documentation:
  - Project-Wiki: [https://github.com/HB-GSI/CSPP/wiki](https://github.com/HB-GSI/CSPP/wiki)
  - NI Actor Framework: [https://decibel.ni.com/content/groups/actor-framework-2011?view=overview](https://decibel.ni.com/content/groups/actor-framework-2011?view=overview)

GIT Submodules
==============
This package can be used as submodule.
- [https://git.gsi.de/EE-LV/CSPP/GSI_VAC/CSPP_Leyboldxxx2.git](https://git.gsi.de/EE-LV/CSPP/GSI_VAC/CSPP_Leyboldxxx2.git)

External Dependencies
-------------------
- [https://git.gsi.de/EE-LV/CSPP/CSPP_Core.git](https://git.gsi.de/EE-LV/CSPP/CSPP_Core.git): Definition of CS++Device ancestor classes
- For LabVIEW instrument driver refer to ID Network


Author: H.Brand@gsi.de

Copyright 2017  GSI Helmholtzzentrum für Schwerionenforschung GmbH

Planckstr.1, 64291 Darmstadt, Germany

Lizenziert unter der EUPL, Version 1.1 oder - sobald diese von der Europäischen Kommission genehmigt wurden - Folgeversionen der EUPL ("Lizenz"); Sie dürfen dieses Werk ausschließlich gemäß dieser Lizenz nutzen.

Eine Kopie der Lizenz finden Sie hier: http://www.osor.eu/eupl

Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in schriftlicher Form vereinbart, wird die unter der Lizenz verbreitete Software "so wie sie ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN - ausdrücklich oder stillschweigend - verbreitet.

Die sprachspezifischen Genehmigungen und Beschränkungen unter der Lizenz sind dem Lizenztext zu entnehmen.